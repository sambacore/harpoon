from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, ListView

from comics.models import Comic, Scene
from comics.views import SceneDetailView, ComicDetailView
from endless_pagination.views import AjaxListView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'harpoon.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', AjaxListView.as_view(
                    template_name='scene_list.html',
		    page_template='scene_list_page.html',
                    queryset=Scene.objects.all()),
                name='comics_list'),

    url(r'^(?P<slug>[-\w\d]+)/$', SceneDetailView.as_view(), name="scene_detail"),
    url(r'^(?P<scene>[-\w\d]+)/(?P<slug>[-\w\d]+)/$', ComicDetailView.as_view(), name="comic_detail"),
)
			   

    
