from django.contrib import admin

from comics.models import Scene, Comic 

class SceneAdmin(admin.ModelAdmin):
    pass

class ComicAdmin(admin.ModelAdmin):
    pass

admin.site.register(Scene, SceneAdmin)
admin.site.register(Comic, ComicAdmin)


