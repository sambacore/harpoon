from django.shortcuts import render
from django.views.generic import DetailView
from endless_pagination.views import AjaxListView

from comics.models import Scene, Comic


class SceneDetailView(DetailView):
	model = Scene
	
class ComicDetailView(DetailView):
	model = Comic




# Create your views here.
