from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save

from localflavor.us.models import USStateField
from django_facebook.models import FacebookProfile

class Scene(models.Model):
    
    city = models.CharField(max_length=60)
    state = USStateField()
    slug = models.SlugField(editable=False)

    def __unicode__(self):
        return ', '.join([self.city, self.state])

    def save(self):
	self.slug = slugify(self.__unicode__())
	return super(Scene, self).save()

    @models.permalink
    def get_absolute_url(self):
	return ('scene_detail', (), {'slug':self.slug })

class Comic(models.Model):

    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    scene = models.ForeignKey(Scene, null=True, blank=True)
    slug = models.SlugField(editable=False)

    def __unicode__(self):
        return self.user.get_full_name() + ' - ' + ', '.join([self.scene.city, self.scene.state])

    def save(self):
	self.slug = slugify(self.user.get_full_name())
	return super(Comic, self).save()

    @models.permalink
    def get_absolute_url(self):
	return ('comic_detail', (), {'scene':self.scene.slug, 'slug': self.slug })


@receiver(post_save, sender=FacebookProfile)
def create_comic(sender, instance, **kwargs):
    if kwargs['created']:
        comic = Comic.objects.get_or_create(user=instance.user)
        comic.save()


