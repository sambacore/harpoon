from django import template
from django.template.loader import render_to_string
from django.utils.timezone import now
import datetime

from feeds.models import Post, Feed
from local_calendar.models import LocalEvent, LocalOccurrence
from calendarium.models import Occurrence

register = template.Library()



@register.inclusion_tag('header_image.html')
def get_header_image(source=None):
	if type(source) == Occurrence:
		image = source.event.localevent.banner.get_header_photo_url()
		link = source.event.localevent.get_absolute_url()
		description = source.localoccurrence.description or source.event.description
	elif type(source) == Feed:
		image = source.image.get_header_photo_url()
		link = source.get_absolute_url()
		description = source.description
	else:
		next = LocalEvent.featured.get_occurrences(now(), now() + datetime.timedelta(days=356))[0]
		image = next.event.localevent.banner.get_header_photo_url()
		link = next.get_absolute_url()
		description = next.description
	return { 'image': image,
		 'link': link,
		 'description': description,}


@register.tag()
def feed_posts(parser, token):
    bits = token.contents.split()
    if len(bits) > 3:
        raise template.TemplateSyntaxError("%s tag takes up to two arguments" % bits[0])
    elif len(bits) < 2:
        raise template.TemplateSyntaxError("%s tag takes at least one argument" % bits[0])
    return FeedPostsNode(bit[1:])
 
class FeedPostsNode(template.Node):

    def __init__(self, args):
        self.feed = Feed.objects.get(title__startswith=args[0])
        if len(args) > 1:
            self.number = int(args[1])

    def render(self, context):
        posts = Post.live.filter(feed=self.feed)[:self.number]
        return render_to_string('tags/post_list.html', { 'posts': posts })

@register.tag()
def soon_shows(parser, token):
    bits = token.contents.split()
    if len(bits) > 2:
        raise template.TemplateSyntaxError("%s tag takes up to one argument" % bits[0])
    elif len(bits) == 2:
        return SoonShowsNode(bits[1])
    return SoonShowsNode()

class SoonShowsNode(template.Node):

    def __init__(self, num=None):
        if num:
            self.num = int(num)
        else:
            self.num = 10

    def render(self, context):
        soon_shows = LocalEvent.objects.filter(start__gte=datetime.datetime.now()).order_by('start')[:self.num]
        return render_to_string('tags/shows_list.html', { 'shows': soon_shows })


