from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, ListView

from endless_pagination.views import AjaxListView
from feeds.models import Post

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'harpoon.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', AjaxListView.as_view(
                    template_name='main.html',
		    page_template='main_page.html',
                    queryset=Post.live.order_by('pub_date')),
                name='front_page'),
    
    
    url(r'^feeds/', include('feeds.urls')),


    url(r'^calendar/', include('local_calendar.urls')),

   
    url(r'^comics/', include('comics.urls')),

    
    url(r'^photologue/', include('photologue.urls')),

    url(r'^search/', include('search.urls')),
    url(r'^test/$', TemplateView.as_view(template_name='test.html')),
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^facebook/', include('django_facebook.urls')),
    url(r'^accounts/', include('django_facebook.auth_urls')),


)
