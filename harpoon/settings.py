"""
Django settings for harpoon project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['.harpoon-master-env-hhgvhx9pgj.elasticbeanstalk.com',
                 '.harpoon-master-env-hhgvhx9pgj.elasticbeanstalk.com.',]


SECRET_KEY = '1039805709290374asdoeinva083'

# Application definition

INSTALLED_APPS = (
    #Built in
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    
    'harpoon',

    #Third party
    'south',
    'bootstrap3',
    'tinymce',
    'filer',
    'easy_thumbnails',
    'calendarium',
    'photologue',
    'tagging',
    'storages',
    's3_folder_storage',
    'endless_pagination',
    'colorfield',
    'django_facebook',

    #Original
    'feeds',
    'local_calendar',
    'comics',

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

ROOT_URLCONF = 'harpoon.urls'

WSGI_APPLICATION = 'harpoon.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/


SITE_ID = 1

TINYMCE_DEFAULT_CONFIG = {
    'theme' : "advanced",
    'relative_urls' : False,
    'remove_script_host' : False,
    'convert_urls' : False,
    'document_base_url' : "http://www.harpoonpresents.com/",
}

SOUTH_MIGRATION_MODULES = {
        'easy_thumbnails': 'easy_thumbnails.south_migrations',
    }

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS
TEMPLATE_CONTEXT_PROCESSORS += (
	'django_facebook.context_processors.facebook',    
	'django.core.context_processors.request',
)

AUTHENTICATION_BACKENDS = (
    'django_facebook.auth_backends.FacebookBackend',
    'django.contrib.auth.backends.ModelBackend',
)

AUTH_PROFILE_MODULE = 'django_facebook.FacebookProfile'

ENDLESS_PAGINATION_PER_PAGE = 18

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/


DEFAULT_S3_PATH = "media"
DEFAULT_FILE_STORAGE = 's3_folder_storage.s3.DefaultStorage'
STATICFILES_STORAGE = 's3_folder_storage.s3.StaticStorage'
STATIC_S3_PATH = "static"
AWS_STORAGE_BUCKET_NAME = "harpoonpresents"

MEDIA_ROOT = ''
MEDIA_URL = 'http://dfr1ie9bx00ol.cloudfront.net/media/'
STATIC_ROOT = "/%s/" % STATIC_S3_PATH
STATIC_URL = 'http://dfr1ie9bx00ol.cloudfront.net/static/'
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'

if 'RDS_DB_NAME' in os.environ:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.environ['RDS_DB_NAME'],
            'USER': os.environ['RDS_USERNAME'],
            'PASSWORD': os.environ['RDS_PASSWORD'],
            'HOST': os.environ['RDS_HOSTNAME'],
            'PORT': os.environ['RDS_PORT'],
        }
    }

if 'AWS_ACCESS_KEY_ID' in os.environ:
    AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
    AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_KEY']

if 'FACEBOOK_APP_ID' in os.environ:
    FACEBOOK_APP_ID = os.environ['FACEBOOK_APP_ID']
    FACEBOOK_APP_SECRET = os.environ['FACEBOOK_APP_SECRET']



try:
    from harpoon.local_settings import *
except ImportError, e:
    pass
