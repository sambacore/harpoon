from django.template import RequestContext
from django.shortcuts import render_to_response
from django.db.models import Q

from feeds.models import Post, Feed
from local_calendar.models import LocalEvent, LocalOccurrence
from comics.models import Comic


import re


def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string into individual keywords, getting rid of
    unnecessary spaces and grouping quoted words together.'''

    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]



def get_query(query_string, search_fields):

    query = None #Query to search for every search term

    terms = normalize_query(query_string)
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else: query = query | or_query
    return query

def search(request):
    query_string = ''
    found_entries = None
    if ('terms' in request.GET) and request.GET['terms'].strip():
        query_string = request.GET['terms']

        post_query = get_query(query_string, ['title', 'body', 'feed__title', 'feed__description',])
        event_query = get_query(query_string, ['',])
        comic_query = get_query(query_string, ['user__first_name', 'user__last_name', 'scene__city', 'scene__state', 'user__username',])
 
        found_posts = Post.live.filter(post_query).order_by('-pub_date')
	found_occurrences = LocalEvent.public.get_occurrences()
	found_comics = Comic.objects.filter(comic_query)	

    return render_to_response('search/search_results.html',
                            { 'query_string': query_string,
                              'found_entries': found_posts,
			      'found_occurrences': found_occurrences,
			      'found_comics': found_comics, },
)




